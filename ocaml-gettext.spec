Name:           ocaml-gettext
Version:        0.4.2
Release:        1
Summary:        Internationalization library for OCaml
License:        LGPLv2+ with exceptions
URL:            https://github.com/gildor478/ocaml-gettext
Source0:        https://github.com/gildor478/ocaml-gettext/archive/%{name}-%{version}.tar.gz
BuildRequires:  ocaml >= 4.00.1 ocaml-findlib-devel >= 1.3.3-3 ocaml-ocamldoc ocaml-camlp4-devel
BuildRequires:  ocaml-fileutils-devel >= 0.4.4-4 docbook-style-xsl ocaml-dune-devel ocaml-cppo
BuildRequires:  libxslt libxml2 chrpath autoconf automake

%global __ocaml_requires_opts -i Asttypes -i Parsetree
%global __ocaml_provides_opts -i Pr_gettext

%description
The package is used to support for internationalization of Ocaml programs.

%package        devel
Summary:        Development files for ocaml-gettext
Requires:       ocaml-gettext = %{version}-%{release}
Requires:       ocaml-fileutils-devel >= 0.4.0

%description    devel
The ocaml-gettext-devel package contains development and libraries and files for ocaml-gettext.

%prep
%autosetup -p1

sed -i -e 's/batteries//' test/dune
sed -i -e 's/batteries//' test/test-stub/dune
sed -i 's/dev/%{version}/g' configure.ml src/lib/gettext/base/gettextConfigGen.ml
rm -r src/lib/gettext-camomile
rm -r test/test-camomile
sed -i -e 's/camomile//' `find -name dune`

%build
make build

%install
mkdir -p $RPM_BUILD_ROOT%{_libdir}/ocaml
mkdir -p $RPM_BUILD_ROOT%{_bindir}
dune install --destdir=$RPM_BUILD_ROOT
rm -rf $RPM_BUILD_ROOT/%{_libdir}/ocaml/gettext-camomile
rm -rf $RPM_BUILD_ROOT/usr/doc

%files
%doc LICENSE.txt
%{_libdir}/ocaml/gettext
%{_libdir}/ocaml/gettext-stub
%exclude %{_libdir}/ocaml/gettext/*/*.ml
%exclude %{_libdir}/ocaml/gettext/*/*.mli
%exclude %{_libdir}/ocaml/gettext-stub/*.ml
%{_libdir}/ocaml/stublibs/*.so

%files devel
%doc README.md CHANGES.md THANKS TODO.md
%{_libdir}/ocaml/gettext/*/*.ml
%{_libdir}/ocaml/gettext/*/*.mli
%{_libdir}/ocaml/gettext-stub/*.ml
%{_bindir}/ocaml-gettext
%{_bindir}/ocaml-xgettext
%{_mandir}/man1/ocaml-gettext.1*
%{_mandir}/man1/ocaml-xgettext.1*
%{_mandir}/man5/ocaml-gettext.5*

%changelog
* Tue Sep 19 2023 liyanan <thistleslyn@163.com> - 0.4.2-1
- update to 0.4.2

* Wed Aug 24 2022 wangkai <wangkai385@h-partners.com> - 0.3.7-9
- Enable debuginfo for fix strip

* Tue Mar 17 2020 Ling Yang <lingyang2@huawei.com> - 0.3.7-8
- Remove camomile package

* Thu Feb 27 2020 zhujunhao <zhujunhao5@huawei.com> - 0.3.7-7
- Package init
